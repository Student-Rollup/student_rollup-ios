//
//  MenuViewController.swift
//  DOAN_iOS_
//
//  Created by Nguyen Dang Thien Tam on 10/25/18.
//  Copyright © 2018 Nguyen Dang Thien Tam. All rights reserved.
//
import AlamofireImage
import UIKit

class MenuViewController: UIViewController, UITableViewDelegate , UITableViewDataSource {
    
    var cellID: [String] = ["cellViewListClass","cellViewDiemDanhDiHoc","cellViewDiemDanhDiThi","cellLogout"]
    var cellValue : [String] = ["Xem Kết Quả Điểm Danh","Điểm Danh Lên Lớp","Điểm Danh Phòng Thi","Đăng Xuất"]
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID[indexPath.row], for: indexPath)
        cell.textLabel?.text = cellValue[indexPath
        .row]
        cell.textLabel?.textColor = UIColor.white


        return cell
    }
    

    @IBOutlet weak var nameProfile: UILabel!
    @IBOutlet weak var idProfile: UILabel!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var myTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
            myTableView.delegate = self
            myTableView.dataSource = self
        myTableView.tableFooterView = UIView.init()
        imageProfile.layer.cornerRadius = imageProfile.frame.size.width / 2
        self.imageProfile.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }
    

   

}
